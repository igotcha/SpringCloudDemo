package top.igotcha.consumer.client.fallback;

import org.springframework.stereotype.Component;
import top.igotcha.consumer.client.UserClient;
import top.igotcha.consumer.pojo.User;

/**
 * @author Gotcha
 * @date 2021/1/26
 * @describe 用于Feign熔断的回调
 */
@Component
public class UserClientFallback implements UserClient {
    @Override
    public User queryById(Long id) {
        User user = new User();
        user.setId(id);
        user.setName("用户异常");
        return user;
    }
}

