package top.igotcha.consumer.client;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import top.igotcha.consumer.client.fallback.UserClientFallback;
import top.igotcha.consumer.config.FeignConfig;
import top.igotcha.consumer.pojo.User;

/**
 * @author Gotcha
 * @date 2021/1/26
 * @describe
 */
@FeignClient(value = "user-service",fallback = UserClientFallback.class,configuration = FeignConfig.class)
public interface UserClient {

    @GetMapping("/user/{id}")
    User queryById(@PathVariable("id") Long id);
}
