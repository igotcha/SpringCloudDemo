package top.igotcha.user.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import top.igotcha.user.mapper.UserMapper;
import top.igotcha.user.pojo.User;

/**
 * @author Gotcha
 * @date 2021/1/25
 * @describe
 */
@Service
public class UserService {
    @Autowired
    private UserMapper userMapper;
    public User queryById(Long id){
        return userMapper.selectByPrimaryKey(id);
    }
}