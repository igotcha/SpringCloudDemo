package top.igotcha.user.mapper;

import tk.mybatis.mapper.common.Mapper;
import top.igotcha.user.pojo.User;

/**
 * @author Gotcha
 * @date 2021/1/25
 * @describe
 */
public interface UserMapper extends Mapper<User> {
}

